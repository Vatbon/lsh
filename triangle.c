#include <stdio.h>

int main() {
    int a, b, c;
    scanf("%d", &a);
    scanf("%d", &b);
    scanf("%d", &c);
    int hold;
    // сортируем стороны в порядке возрастания a > b > c
    if (a > b) {
        hold = a;
        a = b;
        b = hold;
    }
    if (a > c) {
        hold = a;
        a = c;
        c = hold;
    }
    if (b > c) {
        hold = b;
        b = c;
        c = hold;
    }

    if (a + b <= c) {
        printf("impossible\n");
    } else {
        a = a * a;
        b = b * b;
        c = c * c;

        int diff = c - (b + a); // это разница квадратов сторон
        if (diff == 0) { 
            printf("right\n");
        } else if (diff > 0) {
            printf("obtuse\n");
        } else {
            printf("acute\n");
        }
    }
    return 0;
}