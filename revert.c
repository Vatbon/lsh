#include <stdio.h>

int main() {
    int n;
    int arr[10000];
    scanf("%d", &n);
    for(int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }
    int hold;
    for(int i = 0; i < n / 2; i++) {
        hold = arr[i];
        arr[i] = arr[n - i - 1];
        arr[n - i - 1] = hold;
    }
    for(int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    return 0;
}