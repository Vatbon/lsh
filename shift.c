#include <stdio.h>

int main() {
    int n;
    int arr[10000];
    scanf("%d", &n);
    for(int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }
    int hold = arr[n - 1];
    for(int i = n - 1; i > 0; i--) {
        arr[i] = arr[i - 1];
    }
    arr[0] = hold;
    for(int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    return 0;
}