#include <stdio.h>

int main() {
    int n, x, count = 0;
    int arr[10000];
    scanf("%d", &n);
    for(int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }
    scanf("%d", &x);
    for(int i = 0; i < n; i++) {
        if (arr[i] == x) {
            count++;
        }
    }
    printf("%d\n", count);
    return 0;
}