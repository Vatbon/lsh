#include <stdio.h>
#include <math.h>

int main() {
    int a, b, c;
    scanf("%d", &a);
    scanf("%d", &b);
    scanf("%d", &c);
    double d = b * b - 4 * a * c;
    if (d < 0) {
    } // ничего не выводим
    else if (d > 0){ // два корня
        printf("%lf\n", (- b - sqrt(d)) / (2 * a));
        printf("%lf\n", (- b + sqrt(d)) / (2 * a));
    } else { // иначе только один корень
        printf("%lf\n", (double)(- b) / (2 * a));
    }
    return 0;
}