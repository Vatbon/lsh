#include <stdio.h>

int main() {
    int a, b, c;
    scanf("%d", &a);
    scanf("%d", &b);
    scanf("%d", &c);
    char odd = 0,
    even = 0; 
    if (a % 2 == 0 || b % 2 == 0 || c % 2 == 0) {
        even = 1; //если есть хоть один четный
    }
    if (a % 2 == 1 || b % 2 == 1 || c % 2 == 1) {
        odd = 1; //если есть хоть один нечетный
    }
    if (odd && even) {
        printf("YES\n");
    } else {
        printf("NO\n");
    }
    return 0;
}