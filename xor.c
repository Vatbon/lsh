#include <stdio.h>

char Xor(int x, int y) {
    x = x != 0; // Решил сделать доп. проверку, что числа не просто равны 0 или 1
    y = y != 0;
    if (x == y) {
        return 0;
    } 
    return 1;
}

int main() {
    int x, y;
    scanf("%d %d", &x, &y);
    printf("%d\n", Xor(x, y));
    return 0;
}