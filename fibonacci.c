#include <stdio.h>

int main() {
    int n, i = 2;
    scanf("%d", &n);
    unsigned long a = 1,
        b = 1,
        hold;
    if (n == 0) {
        printf("0\n");
    } else if (n == 1) {
        printf("1\n");
    } else {
        while (n > b) {
            hold = b;
            b = b + a;
            a = hold;
            i++;
        }
        if (n == b) {
            printf("%d\n", i);
        } else {
            printf("-1\n");
        }
    }
    return 0;
}