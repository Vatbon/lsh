#include <stdio.h>

unsigned long phi(int n) {
    unsigned long a = 1,
        b = 1,
        hold;
    if (n == 0 || n == 1) {
        return 1;
    }
    for (int i = 0; i < n - 1; i++) {
        hold = b;
        b = b + a;
        a = hold;
    }
    return b;
}

int main() {
    int n;
    scanf("%d", &n);
    printf("%ld\n", phi(n));
    return 0;
}