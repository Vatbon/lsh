#include <stdio.h>

unsigned long binomial(int n, int k) {
    if (n == k || k == 0) {
        return 1;
    }
    return binomial(n - 1, k - 1) + binomial(n - 1, k);
}

int main() {
    int n, k;
    scanf("%d %d", &n, &k);
    printf("%ld\n", binomial(n, k));
    return 0;
}