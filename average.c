#include <stdio.h>

int main() {
    int a;
    int sum = 0, count = 0;
    scanf("%d", &a);
    while (a) {
        sum = sum + a;
        count++;
        scanf("%d", &a);
    }
    if (count) {
        printf("%lf\n", (double)sum / (double)count);
    }
    return 0;
}