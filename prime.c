#include <stdio.h>
#include <math.h>

char prime(int n) {
    for(int i = 2; i < (int)sqrt(n) + 1; i++) {
        if (n % i == 0) {
            return 0;
        }
    }
    return 1;
}

int main() {
    int n;
    scanf("%d", &n);
    if (prime(n)) {
        printf("prime\n");
    } else {
        printf("composite\n");
    }
    return 0;
}