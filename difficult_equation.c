#include <stdio.h>

int main() {
    int a, b, c, d;
    scanf("%d", &a);
    scanf("%d", &b);
    scanf("%d", &c);
    scanf("%d", &d);
    int root;
    if (a == 0 && b == 0) {
        printf("INF\n");
    } else if (a == 0 && b != 0) {
        printf("NO\n");
    } else {
        if (b % a != 0) {
            printf("NO\n");
        } else {
            root = - b / a;
        }
        if (c != 0) {
            if (d % c == 0) {
                if (root == - d / c) {
                    printf("NO\n");
                } else {
                    printf("%d\n", root);
                }
            } else {
                printf("%d\n", root);
            }
        }
    }
    return 0;
}