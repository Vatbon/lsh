#include <stdio.h>

int main() {
    int n, count = 0;
    int arr[10000];
    scanf("%d", &n);
    for(int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }
    for(int i = 1; i < n; i++) {
        if (arr[i - 1] < arr[i]) {
            count++;
        }
    }
    printf("%d\n", count);
    return 0;
}